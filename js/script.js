//GLOBAL VARS
var payeeName;
var payeeBIC;
var payeeReference;
var payeeType;
var payeeIBAN;
var payeeDescription;
var studentID;
var studentPin;
var currentBalance = 1520.50;
var $id = function( id ) { return document.getElementById( id ); };
var transferFrom;
var transferTo;
var transferAmount;

function init(){
    console.log("THIS TITLE "+document.title);
    switch(document.title) {
    case "Dashboard":     
        document.getElementById("currentBalance").innerHTML = "€"+currentBalance;
        console.log("IN HERE"+ currentBalance); 
        break; 
    case "Transfers":     
        document.getElementById("currentBalance").innerHTML = "€"+currentBalance;
        console.log("IN HERE"+ currentBalance); 
        break;
    case "Confirm Transfer":
        console.log("Cookie"+ getCookie("transferFrom")); 
        document.getElementById("currentBalance").innerHTML = "€"+currentBalance;    
        document.getElementById("transferFromChoice").innerHTML = getCookie("transferFrom"); 
        document.getElementById("transferToChoice").innerHTML = getCookie("transferTo"); 
        document.getElementById("transferAmount").innerHTML = "€"+getCookie("transferAmount");
        document.getElementById("updatedBalance").innerHTML =  "€"+(currentBalance - getCookie("transferAmount")); 
        break;
    case "Transfer Complete":
        console.log("Cookie"+ getCookie("transferFrom")); 
        document.getElementById("currentBalance").innerHTML = "€"+currentBalance;    
        document.getElementById("transferFromChoice").innerHTML = getCookie("transferFrom"); 
        document.getElementById("transferToChoice").innerHTML = getCookie("transferTo"); 
        document.getElementById("transferAmount").innerHTML = "€"+getCookie("transferAmount");
        document.getElementById("updatedBalance").innerHTML =  "€"+(currentBalance - getCookie("transferAmount")); 
        break; 
    case "Add a Payee":
        console.log("Add a Payee"+ getCookie("transferFrom")); 
        document.getElementById("currentBalance").innerHTML = "€"+currentBalance;    
        break; 
    case "Confirm Payee":
        console.log("Confirm Payee"+ getCookie("payeeReference")); 
        document.getElementById("currentBalance").innerHTML = "€"+currentBalance;  
        document.getElementById("BIC").innerHTML = getCookie("BIC");  
        document.getElementById("payeeName").innerHTML = getCookie("payeeName");      
        document.getElementById("payeeType").innerHTML = getCookie("payeeType");      
        document.getElementById("IBAN").innerHTML = getCookie("payeeIBAN");      
        document.getElementById("reference").innerHTML = getCookie("payeeReference");      
        document.getElementById("description").innerHTML = getCookie("payeeDescription");      
        break;
    case "Payee Complete":
        document.getElementById("currentBalance").innerHTML = "€"+currentBalance;  
        document.getElementById("BIC").innerHTML = getCookie("BIC");  
        document.getElementById("payeeName").innerHTML = getCookie("payeeName");      
        document.getElementById("payeeType").innerHTML = getCookie("payeeType");      
        document.getElementById("IBAN").innerHTML = getCookie("payeeIBAN");      
        document.getElementById("reference").innerHTML = getCookie("payeeReference");      
        document.getElementById("description").innerHTML = getCookie("payeeDescription");      
        break;         
    default:
        
        getCookie();
}
            
    
}

/*function checkLogin(){
    var studentID = $id('student-id').value;
    var studentPin = $id('student_pin_2').value + $id('student_pin_4').value + $id('student_pin_6').value;
    console.log("studentID "+studentID + "studentPin "+studentPin);
    if((studentID == 123456) && (studentPin == 666)){
        console.log("CORRECT");
        window.open("dashboard.html","_self");
    }else{
        console.log("INCORRECT"); 
        document.getElementById("error").innerHTML = "Sorry your Student ID or PIN was incorrect";
    }
    return false;
}*/

function transfersChanged(){
    var transferFrom = $id("transfer-from").value;
    var transferTo = $id("transfer-to").value;
    var transferAmount = $id("amount").value;
    console.log(transferFrom);
    document.cookie="transferFrom="+transferFrom+";";
    document.cookie="transferTo="+transferTo+";";
    document.cookie="transferAmount="+transferAmount+";";
}


function formChanged(){
    payeeBIC = document.getElementById("BIC").value;
    payeeName = document.getElementById("payeeName").value;
    payeeType = document.getElementById("payeeType").value;
    payeeIBAN = document.getElementById("IBAN").value;
    payeeReference= document.getElementById("reference").value;
    payeeDescription = document.getElementById("description").value;
    document.cookie="BIC="+payeeBIC+";";
    document.cookie="payeeName="+payeeName+";";
    document.cookie="payeeType="+payeeType+";";
    document.cookie="payeeIBAN="+payeeIBAN+";";
    document.cookie="payeeReference="+payeeReference+";";
    document.cookie="payeeDescription="+payeeDescription+";";
    //alert("HERE")
}

function displayCookies() {
    var fname=getCookie("BIC");
    if (fname==null) {fname="";}
    if (fname!="") {fname="BIC="+fname;}
    //alert ("fname "+fname);
}


function getCookie(name) {
    var nameEQ = name + "=";
    //alert(document.cookie);
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(nameEQ) != -1) return c.substring(nameEQ.length,c.length);
    }
    return null;
}